import java.io.*;
import java.util.*;

import javax.swing.*;
public class LR_Parsing 
{
	private static void promtErrorMessage(String message)
	{
		JOptionPane.showMessageDialog(null,message);
		System.exit(-1);
	}
	public static void main(String[] args) 
	{
		String Gfilename = JOptionPane.showInputDialog("enter the grammar file name!");
		String Sfilename = JOptionPane.showInputDialog("enter the string file name!");
		ArrayList<GrammarNode> ruleList = new ArrayList<GrammarNode>();
		int count = -1;
		try 
		{
			Scanner line = new Scanner(new File(Gfilename));
			while(line.hasNextLine())
			{
				Scanner word = new Scanner(line.nextLine());
				while(word.hasNext())
				{
					String nextword = word.next();
					int condition;
					if(nextword.compareTo("|")==0)
						condition = 2;
					else
						condition = 1;

					switch(condition)
					{
						case 2:
						{
							if(!(word.hasNext()))
								promtErrorMessage("Invalid grammar format, please check your grammar file1: "+Gfilename);
							if(!(count>=0))
								promtErrorMessage("Invalid grammar format, please check your grammar file2: "+Gfilename);
							String temp ="";
							while(word.hasNext())
							{
								nextword=word.next();
								temp=temp+nextword; //------------- " "
							}
								temp=temp.substring(0,temp.length());
								GrammarNode head = ruleList.get(count);
								while(head!=null&&head.getNext()!=null)
									head=head.getNext();
								if(head==null)
									promtErrorMessage("Invalid grammar format, please check your grammar file3: "+Gfilename);
								else
									head.setNext(new GrammarNode(temp));
							break;
						}
						case 1:
						{
							ruleList.add(new GrammarNode(nextword));
							count+=1;
							String temp = "";
							while(word.hasNext())
							{
								nextword=word.next();
								if(nextword.compareTo("=")==0)
									if(word.hasNext())
										nextword=word.next();
								temp=temp+nextword;  // ---------------------
							}
							temp=temp.substring(0,temp.length());
								GrammarNode head = ruleList.get(count);
								while(head!=null&&head.getNext()!=null)
									head=head.getNext();
								if(head==null)
									promtErrorMessage("Invalid grammar format, please check your grammar file3: "+Gfilename);
								else
									head.setNext(new GrammarNode(temp));
								break;
						}
					}
				}
				word.close();
			}
			// testing the grammarList to see if it works right:
			System.out.println("Here is the grammar:");
			for(GrammarNode temp:ruleList)
			{
				while(temp!=null)
				{
					System.out.print(temp.toString());
					temp=temp.getNext();
				}
				System.out.println();
			}
			line.close();
		} 
		catch (FileNotFoundException e) 
		{
			JOptionPane.showMessageDialog(null, " grammar file:, "+Gfilename+" is not found!");
			System.exit(-1);
		}
		Stack<String> stringstack = new Stack<String>();
		try
		{
			Scanner line = new Scanner(new File(Sfilename));
			Scanner word = new Scanner(line.nextLine());
			while(word.hasNext())
			{
				String nextword = word.next();
				stringstack.push(nextword);
				stringstack = check(stringstack,ruleList);
			}
			if(stringstack.size()==1&&stringstack.peek().trim().compareTo(ruleList.get(0).get_grammar_Name().trim())==0)
				JOptionPane.showMessageDialog(null,"valid grammar, yeah!");
			else
				JOptionPane.showMessageDialog(null,"Invalid grammar!");
			line.close();
			word.close();
			System.exit(0);
		}
		catch(FileNotFoundException e)
		{
			JOptionPane.showMessageDialog(null, " grammar file:, "+Sfilename+" is not found!");
			System.exit(-1);
		}
	}
	private static Stack<String> check(Stack<String> input, ArrayList<GrammarNode> rule)
	{
		ArrayList<String> temp = new ArrayList<String>();
		String word = "";
		int condition = 0;
		int jump=1;
		while(!input.isEmpty())              // looping through the stack
		{
			condition = 0;
			String cat = input.pop();
			word=cat+word; // ------------- " ";
			temp.add(cat);
			word = word.trim();
			for(int i=0;i<rule.size();i++)           // looping through the rule array list
			{
				GrammarNode head = rule.get(i).getNext();
				while(head!=null)                // looping through  the rule arrayList[i] 
				{
					if(head.get_grammar_Name().trim().compareTo(word)==0)
					{
						condition = 1;
						break;
					}
					head =head.getNext();
				}
				if(condition>0)
				{
					input.push(rule.get(i).get_grammar_Name());
					temp=new ArrayList<String>();
					jump=0;
					break;
				}
			}
		}
			for(int k=temp.size()-1;k>=0;k--)
			{
				input.push(temp.get(k));
			}
			if(jump==0)
			check(input,rule);
		return input;
	}

}
